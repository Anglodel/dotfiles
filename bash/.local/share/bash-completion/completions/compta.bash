#/usr/bin/env bash
_compta_completions()
{
  if [ "${#COMP_WORDS[@]}" != "2" ]; then
    return
  fi

  COMPREPLY=($(compgen -W "open close" -- "${COMP_WORDS[1]}"))
}

complete -F _compta_completions compta
