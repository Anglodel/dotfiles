// =============================================================================
//
// Waybar configuration
//
// Configuration reference: https://github.com/Alexays/Waybar/wiki/Configuration
//
// =============================================================================

{
    // -------------------------------------------------------------------------
    // Global configuration
    // -------------------------------------------------------------------------

    "layer": "top",

    "position": "bottom",

    // If height property would be not present, it'd be calculated dynamically
    "height": 22,

    "modules-left": [
        "disk#root",
        "disk#home",
        "battery",
        "cpu",
        "memory",
        "temperature"
    ],
    "modules-center": [
        "sway/workspaces",
        "sway/mode"
    ],
    "modules-right": [
        "custom/power",
        "mpd",
        "pulseaudio",
        "clock",
        "custom/vpn",
        "network#all",
        "tray"
    ],


    // -------------------------------------------------------------------------
    // Modules
    // -------------------------------------------------------------------------

    "battery": {
        "interval": 30,
        "design-capacity": true,
        "states": {
            "warning": 40,
            "critical": 30
        },
        // Connected to AC
        "format": " {capacity}%", // Icon: plug
        // Not connected to AC
        "format-discharging": "{icon} {capacity}%",
        "format-icons": [
            "", // Icon: battery-full
            "", // Icon: battery-three-quarters
            "", // Icon: battery-half
            "", // Icon: battery-quarter
            ""  // Icon: battery-empty
        ],
        "tooltip": true
    },

    "clock": {
      //"interval": 10,
      "format": " {:%a %d/%m %H:%M}", // Icon: calendar-alt
      "tooltip-format": "<big>{:%A %d %B %Y}</big>\n<tt><small>{calendar}</small></tt>"
    },

    "cpu": {
        "interval": 6,
        "format": " {usage}%", // Icon: microchip
        "states": {
          "warning": 70,
          "critical": 90
        }
    },

    "memory": {
        "interval": 10,
        "format": " {}%", // Icon: memory
        "tooltip-format": "{used:0.1f} GiB / {total:0.1f} GiB",
        "states": {
            "warning": 70,
            "critical": 85
        }
    },

    "network#all": {
        "interval": 5,
        "format-wifi": " {signalStrength}%", // Icon: wifi
        "format-ethernet": " Up", // Icon: ethernet
        "format-disconnected": "⚠ Down",
        "tooltip-format-wifi": "Wifi : {essid} - {ifname}: {ipaddr}/{cidr}",
        "tooltip-format-ethernet": "Ethernet : {ifname}: {ipaddr}/{cidr}",
        "tooltip-format-disconnected": "Disconnected",
    },
    "custom/vpn": {
        "interval": 5,
        "tooltip": "false",
        "format": "{}",
        "return-type": "json",
        "exec": "check_vpn"
    },

    "sway/mode": {
        "format": " <span style=\"italic\">{}</span>", // Icon: expand-arrows-alt
        "tooltip": false
    },

    "sway/workspaces": {
        "all-outputs": false,
        "disable-scroll": true,
        "format": "{name}"
    },

    "temperature": {
      "critical-threshold": 70,
      "interval": 5,
      "format": "{icon} {temperatureC}°C",
      "format-icons": [
          "", // Icon: temperature-empty
          "", // Icon: temperature-quarter
          "", // Icon: temperature-half
          "", // Icon: temperature-three-quarters
          ""  // Icon: temperature-full
      ],
      "tooltip": true
    },

    "tray": {
        //"icon-size": 21,
        "spacing": 10
    },
    "pulseaudio": {
        // "scroll-step": 1, // %, can be a float
        "format": "{icon} {volume}%",
        "format-muted": " ",
        "format-icons": {
            "headphone": "",
            "hands-free": "",
            "headset": "",
            "phone": "",
            "portable": "",
            "car": "",
            "default": ["", "", ""]
        },
        "on-click": "pavucontrol"
    },
    "disk#root": {
        "interval": 60,
        "format": " / {free}",
        "tooltip-format": "Util. {path} : {used} / {total} ({percentage_used}%)",
        "path": "/"
    },
    "disk#home": {
        "interval": 60,
        "format": " ~ {free}",
        "tooltip-format": "Util. {path} : {used} / {total} ({percentage_used}%)",
        "path": "/home"
    },

    "mpd": {
        "format": "♪ {stateIcon} {consumeIcon}{randomIcon}{repeatIcon}{singleIcon}",
        "format-stopped": "♪ ",
        "format-disconnected": "MPD ?",
        "interval": 10,
        "consume-icons": {
            "on": "" // Icon shows only when "consume" is on
        },
        "random-icons": {
            "on": ""
        },
        "repeat-icons": {
            "on": ""
        },
        "single-icons": {
            "on": "1"
        },
        "state-icons": {
            "paused": "",
            "playing": ""
        },
        "tooltip-format": "{artist} : {title} - {album} - {elapsedTime:%M:%S}/{totalTime:%M:%S}",
        "tooltip-format-disconnected": "Mpd est déconnecté ..."
    },

    "custom/power": {
        "format": "",
        "tooltip": false,
        "on-click": "~/bin/bemenu-power"
    }

}
